// No. 03

db.rooms.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms available": 10,
	"isAvailable": false
});



// No. 04
db.rooms.insertMany([
	{
		"name": "double",
		"accomodates": 3,
		"price": 2000,
		"description": "A room fit for a smalll family going on vaccation",
		"rooms available": 15,
		"isAvailable": false
	}
	{
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room with a queen size bed perfect for a simple getaway",
		"rooms available": 15,
		"isAvailable": false
	}
]);



// No. 05

db.rooms.find({"name": "double"});


// No. 06
db.rooms.updateOne({"name": "queen"},{
	$set: {"rooms_available": 0}
});



// No. 07

db.rooms.deleteMany({"rooms_available": 0});